(function() {
  'use strict';

  angular
    .module('mioCc', ['ngAnimate', 'ngTouch', 'ngSanitize', 'ui.router', 'ui.bootstrap']);

})();
