(function() {
    'use strict';

    angular
        .module('mioCc')
        .controller('ChallengeController', ChallengeController);

    /** @ngInject */
    function ChallengeController($http) {
        var vm = this;
        var url = 'data/big_data.json';

        vm.carMovements = [];
        vm.counts = {
            'left': 0,
            'right': 0,
            'straight': 0,
            'uTurn': 0,
            'unknown': 0
        };


        $http.get(url).then(function(response) {
            vm.detections = _.sortBy(response.data, 'time'); //could remove since sorted below
            vm.groupedDetections = _.groupBy(vm.detections, 'vehicle');
            vm.uniqueVehicleCount = Object.keys(vm.groupedDetections).length;

            vm.inIntersection = [];
            vm.multipleAppearances = [];

            _.each(vm.groupedDetections, function(vehicleDetections) {
                var sortedDetections = _.sortBy(vehicleDetections, 'time'); //ensure detections are sorted by time

                var len;
                if (sortedDetections.length % 2 === 0) {
                    len = sortedDetections.length;
                } else {
                    len = sortedDetections.length - 1;
                    vm.inIntersection.push(sortedDetections[0].vehicle);
                }
                if (sortedDetections.length > 2) {
                    vm.multipleAppearances.push(sortedDetections[0].vehicle);
                }


                var o, d, movement;
                for (var x = 0; x < len; x = x + 2) {

                    if (sortedDetections[x].vehicle === sortedDetections[x + 1].vehicle) {

                        o = sortedDetections[x].region;
                        d = sortedDetections[x + 1].region;

                        if ((o === 1 && d === 6) || (o === 2 && d === 7) || (o === 3 && d === 8) || (o === 4 && d === 5)) {
                            movement = 'right';
                            vm.counts.right++;
                        } else if ((o === 1 && d === 7) || (o === 2 && d === 8) || (o === 3 && d === 5) || (o === 4 && d === 6)) {
                            movement = 'straight';
                            vm.counts.straight++;
                        } else if ((o === 1 && d === 8) || (o === 2 && d === 5) || (o === 3 && d === 6) || (o === 4 && d === 7)) {
                            movement = 'left';
                            vm.counts.left++;
                        } else if ((o === 1 && d === 5) || (o === 2 && d === 6) || (o === 3 && d === 7) || (o === 4 && d === 8)) {
                            movement = 'u-turn';
                            vm.counts.uTurn++;
                        } else {
                            movement = 'unknown/collision/wrongway';
                            vm.counts.unknown++;
                        }
                    }

                    vm.carMovements.push({
                        vehicle: sortedDetections[x].vehicle,
                        origin: o,
                        destination: d,
                        startTime: sortedDetections[x].time,
                        endTime: sortedDetections[x + 1].time,
                        type: movement
                    });
                }
            });
        });
    }
})();